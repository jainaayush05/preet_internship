# Imports
from __future__ import division
import nltk.corpus
from nltk import word_tokenize
import nltk.stem.snowball
from nltk.corpus import wordnet
import string
from spiral import ronin
from nltk.corpus import wordnet as wn
from multiprocessing.dummy import Pool as ThreadPool
import sys
import csv
from sklearn.feature_extraction import text
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np

stopwords = nltk.corpus.stopwords.words('english')
stopwords.extend(string.punctuation)
stopwords.append('')

lemmatizer = nltk.stem.wordnet.WordNetLemmatizer()

def get_wordnet_pos(pos_tag):
    if pos_tag[1].startswith('J'):
        return (pos_tag[0], wordnet.ADJ)
    elif pos_tag[1].startswith('V'):
        return (pos_tag[0], wordnet.VERB)
    elif pos_tag[1].startswith('N'):
        return (pos_tag[0], wordnet.NOUN)
    elif pos_tag[1].startswith('R'):
        return (pos_tag[0], wordnet.ADV)
    else:
        return (pos_tag[0], wordnet.NOUN)

def sentence_modifier(a):
    # breakdown complex words to simpler ones: footwear -> foot wear
    # using https://github.com/casics/spiral
    ronin.init(low_freq_cutoff=10)

    #tokenization & lemmatization
    pos_a = map(get_wordnet_pos, nltk.pos_tag(ronin.split(a)))
    lemmae_a = [lemmatizer.lemmatize(token.lower().strip(string.punctuation), pos) for token, pos in pos_a \
                    if token.lower().strip(string.punctuation) not in stopwords]
    return " ".join(lemmae_a)

# go through dictionary.csv, modify all of it and make a dictionary (map) of processed -> original

file = open('dictionary.csv','r')
new2old = dict()

while (True):

    x = file.readline()
    if (x==""):
        break
    # print (x)
    x = x.strip()
    modified = sentence_modifier(x).strip()
    new2old[modified] = x

file.close()

file = open('target.csv','r')
finalArr = []

while (True):
    x = file.readline()
    if (x==""):
        break

    x = x.strip()
    # print (x)
    # print (new2old[x])
    try:
        finalArr.append(new2old[x])
    except:
        print (x)

file.close()
print (len(finalArr))

file = open('correctOrder.csv','w')

for val in finalArr:
    file.write(val+'\n')

file.close()