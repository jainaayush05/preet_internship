import sys
sys.path.append('../')
from text_similarity import TextSimilarity
from text_similarity import GoogleTranslator
import csv

target_filename = "target" #without .csv extension
dictionary_filename = "dictionary" #without .csv extension
output_filename = "mapping" #without .csv extension
already_translated = True

#translating if not already_translated
if not already_translated:
    text_arr = []
    with open(target_filename + ".csv", 'r') as csvfile_t:
        targetcsv = csv.reader(csvfile_t, delimiter=',')
        for target in targetcsv:
            text_arr.append(target[0])

    translator = GoogleTranslator(text_arr, "paid", 'de' ,50)
    translated_arr= translator.translate()
    #saving translation
    with open(target_filename + "_en.csv", 'w') as csvfile:
       writer = csv.writer(csvfile, delimiter=',')
       for trans in translated_arr:
           writer.writerow([trans])

#mapping with dict
ts = TextSimilarity(target_filename + "_en.csv",dictionary_filename + ".csv",output_filename + ".csv")
ts.run()
