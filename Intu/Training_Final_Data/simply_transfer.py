from comet_ml import Experiment
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras.models import Model
# import matplotlib.pyplot as plt
from keras.regularizers import l2
import pickle

# Add the following code anywhere in your machine learning file
experiment = Experiment(api_key="0Tr7vcQXRVEm9oKolRUGMCrgu", project_name="intu", team_name="greendeck")

train_data_dir  = 'Intu1-Data/train'
validation_data_dir = 'Intu1-Data/validation'

img_width, img_height = 150, 150

top_model_weights_path = 'InceptionResNetV2_transfer_model.h5'

epochs = 50
batch_size = 64

datagen = ImageDataGenerator(
        rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest')

test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = datagen.flow_from_directory(
	train_data_dir,
	target_size=(img_width, img_height),
	batch_size=batch_size,
	class_mode="categorical",
	shuffle=True)

validation_generator = test_datagen.flow_from_directory(
	validation_data_dir,
	target_size=(img_width, img_height),
	batch_size=batch_size,
	class_mode="categorical",
	shuffle=True)

base_model = InceptionResNetV2(include_top=False, pooling='avg', weights='imagenet')
outputs = Dense(5, activation='softmax',kernel_regularizer=regularizers.l2(0.01))(base_model.output)

# outputs = Dropout(0.5)(outputs)

model = Model(base_model.inputs, outputs)
model.compile(optimizer=optimizers.SGD(lr=1e-4, momentum=0.9), loss = 'categorical_crossentropy', metrics = ['categorical_accuracy', 'accuracy'])

#class_weights ==> use alpha numeric order... 

class_weight = {0:3,1:3,2:1,3:3,4:3} #acces, bags, cloth, foot, jwellery

# model.fit_generator(train_generator, steps_per_epoch=nb_train_samples // batch_size, epochs = epochs, validation_data=validation_generator,validation_steps=nb_validation_samples  // batch_size, class_weight=class_weight)
model.fit_generator(train_generator, epochs = epochs, validation_data=validation_generator, class_weight=class_weight)

model.save(top_model_weights_path)

# print(history.history.keys())
# # summarize history for accuracy
# plt.plot(history.history['acc'])
# plt.plot(history.history['val_acc'])
# plt.title('model accuracy')
# plt.ylabel('accuracy')
# plt.xlabel('epoch')
# plt.legend(['train', 'validation'], loc='upper left')
# plt.show()
# # summarize history for loss
# plt.plot(history.history['loss'])
# plt.plot(history.history['val_loss'])
# plt.title('model loss')
# plt.ylabel('loss')
# plt.xlabel('epoch')
# plt.legend(['train', 'validation'], loc='upper left')
# plt.show()

with open('/trainHistoryDict', 'wb') as file_pi:
    pickle.dump(model.history, file_pi)