from keras.models import load_model
from keras.preprocessing import image
import numpy as np
import os

model = load_model('./../../../model-improvement-01-0.91.h5')
# model.compile(optimizer=optimizers.SGD(lr=1e-4, momentum=0.9), loss = 'categorical_crossentropy', metrics = ['categorical_accuracy', 'accuracy'])

test_path = './Intu1-Data/test'
folders = os.listdir(test_path)
folders.sort()

int2class = {0:'Accessories', 1:'Bags', 2:'Clothing', 3:'Footwear', 4:'Jewellery'}

accuracy_file = file.open('accuracy_overall.csv','w')

for i in range(0,len(folders)):

	false_file = file.open(folders[i]+'_falsely_predicted.csv','w')

	folder_path = test_path + '/' + folders[i]

	all_images = os.listdir(folder_path)

	count = 0

	for file in all_images:
		img = image.load_img(folder_path+'/'+file, target_size=(img_width, img_height))

		x = image.img_to_array(img)
	    x = np.expand_dims(x, axis=0)
	    
	    images = np.vstack([x])
	    classes = model.predict_classes(images, batch_size=10)
		classes = classes[0][0]

		if (classes == i):
			count+=1
		else:
			false_file.write(file+','+int2class[classes]+'\n')

	false_file.close()
	accuracy_file.write(folders[i]+','+str(count)+','+len(all_images))

accuracy_file.close()