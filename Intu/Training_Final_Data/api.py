# import the necessary packages
from keras.models import load_model
from keras.preprocessing.image import img_to_array
from keras.applications import imagenet_utils
from PIL import Image
import numpy as np
import flask
import io
from keras.models import Model
from PIL import Image
import requests
from io import BytesIO
from flask import request

# initialize our Flask application and the Keras model
app = flask.Flask(__name__)
model = load_model('./../../../model-improvement-01-0.91.h5')

# print (model.summary())

index2class = {0:'Accessories', 1:'Bags', 2:'Clothing', 3:'Footwear', 4:'Jewellery'}

def prepare_image(image, target):
    # if the image mode is not RGB, convert it
    if image.mode != "RGB":
        image = image.convert("RGB")

    # resize the input image and preprocess it
    image = image.resize(target)
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)
    image = imagenet_utils.preprocess_input(image)

    # return the processed image
    return image

@app.route("/predict", methods=["POST"])
def predict():
    # initialize the data dictionary that will be returned from the
    # view
    data = {"success": False}
    response = ""
    topResponse = ""

    # ensure an image was properly uploaded to our endpoint
    if flask.request.method == "POST":
        url = request.get_json().get('img_url')
        if url:
            # read the image in PIL format
            img_response = requests.get(url)
            image = Image.open(io.BytesIO(img_response.content))
            # preprocess the image and prepare it for classification
            image = prepare_image(image, target=(150, 150))

            # classify the input image and then initialize the list
            # of predictions to return to the client
            preds = model.predict(image)
            print (type(preds))
            print (preds)

            arr = preds[0]
            top = 0
            second_best = ""
            third_best = ""
            for i in range(0,len(arr)):
            	response = response + " , " + index2class[i] + "=" + str(arr[i])
            	if (arr[i]>=top):
                    top = arr[i]
                    third_best = second_best
                    second_best = topResponse
                    topResponse = index2class[i]

            # indicate that the request was a success
            data["success"] = True
            data["response"] = response
            data["1"] = topResponse
            data["2"] = second_best
            data["3"] = third_best

    # return the data dictionary as a JSON response
    return flask.jsonify(data)

# if this is the main thread of execution first load the model and
# then start the server
if __name__ == "__main__":
    print(("* Loading Keras model and Flask starting server..."
        "please wait until server has fully started"))
     # load_model()
    app.run()
