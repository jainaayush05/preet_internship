fileRead = open('mapping.csv','r')
fileWrite = open('df2intu.csv','w')
#mapping1.csv ==> deepFashion, intu, comment (0->cahnged manually, 1-> recommended by text similarity, 2-> changed thorugh this code, ?-> confusion)

x = fileRead.readline()

while (True):

	x = fileRead.readline()
	if (x==""):
		break
	x = x.split(',')

	deepFashion = x[0].strip()
	intu = x[1].strip()
	similarity = x[2].strip()

	if ('kimono' in deepFashion):
		continue

	if ('blouse' in deepFashion):
		fileWrite.write(deepFashion+','+'/Clothing/Tops,2\n')
		continue

	if ('sweater' in deepFashion):
		fileWrite.write(deepFashion+','+'/Clothing,2\n')
		continue

	# covering cases where specific changes need to be made 

	if ('dress' in deepFashion):
		if ("Dresses" not in intu.split('/')):
			if ('body' in deepFashion):
				fileWrite.write(deepFashion+','+'/Clothing/Dresses/Bodycon,2\n')
			elif ('a-line' in deepFashion):
				fileWrite.write(deepFashion+','+'/Clothing/Dresses/A-line,2\n')
			elif ('maxi' in deepFashion):
				fileWrite.write(deepFashion+','+'/Clothing/Dresses/Maxi,2\n')
			elif ('midi' in deepFashion):
				fileWrite.write(deepFashion+','+'/Clothing/Dresses/Midi,2\n')
			elif ('mini' in deepFashion):
				fileWrite.write(deepFashion+','+'/Clothing/Dresses/Mini,2\n')
			elif ('shift' in deepFashion):
				fileWrite.write(deepFashion+','+'/Clothing/Dresses/Shift,2\n')
			elif ('skater' in deepFashion):
				fileWrite.write(deepFashion+','+'/Clothing/Dresses/Skater,2\n')
			else:
				fileWrite.write(deepFashion+','+'/Clothing/Dresses,2\n')
		else:
			fileWrite.write(deepFashion+','+intu+',1\n')
		continue

	if ('skirt' in deepFashion):
		if ('Skirts' not in intu.split('/')):
			if ('midi' in deepFashion):
				fileWrite.write(deepFashion+','+'/Clothing/Skirts/Midi,2\n')
			elif ('long' in deepFashion):
				fileWrite.write(deepFashion+','+'/Clothing/Skirts/Long,2\n')
			elif ('mini' in deepFashion):
				fileWrite.write(deepFashion+','+'/Clothing/Skirts/Mini,2\n')
			else:
				fileWrite.write(deepFashion+','+'/Clothing/Skirts,2\n')
		else:
			fileWrite.write(deepFashion+','+intu+',1\n')
		continue

	if ('short' in deepFashion):
		if ("Shorts" not in intu.split('/')):
			fileWrite.write(deepFashion+','+'/Clothing/Shorts,2\n')
		else:
			fileWrite.write(deepFashion+','+intu+',1\n')
		continue

	if ('jumpsuit' in deepFashion):
		if ("Jumpsuits & Playsuits" not in intu.split('/')):
			fileWrite.write(deepFashion+','+'/Clothing/Jumpsuits & Playsuits,2\n')
		else:
			fileWrite.write(deepFashion+','+intu+',1\n')
		continue

	if ('hoodie' in deepFashion):
		if ("Sweatshirts & Hoodies" not in intu.split('/')):
			fileWrite.write(deepFashion+','+'/Clothing/Sweatshirts & Hoodies,2\n')
		else:
			fileWrite.write(deepFashion+','+intu+',1\n')
		continue

	if ('jacket' in deepFashion):
		if ("Jackets" not in intu.split('/')):
			fileWrite.write(deepFashion+','+'/Clothing/Outerwear/Jackets,2\n')
		else:
			fileWrite.write(deepFashion+','+intu+',1\n')
		continue

	if ('tee' in deepFashion):
		if (intu!='/Clothing/Tops/T-shirts'):
			fileWrite.write(deepFashion+','+'/Clothing/Tops/T-shirts,2\n')
		else:
			fileWrite.write(deepFashion+','+intu+',1\n')
		continue

	if ('top' in deepFashion):
		if ("Tops" not in intu.split('/')):
			fileWrite.write(deepFashion+','+'/Clothing/Tops,2\n')
		else:
			fileWrite.write(deepFashion+','+intu+',1\n')
		continue

	if ('cardigan' in deepFashion):
		if ("Cardigans" not in intu.split('/')):
			fileWrite.write(deepFashion+','+'/Clothing/Knitwear/Cardigans,2\n')
		else:
			fileWrite.write(deepFashion+','+intu+',1\n')
		continue

	# covering the case where manually corrected

	if (len(x[4])>1):
	
		symbol = x[3].strip()
		correctedName = x[4].strip()
		fileWrite.write(deepFashion+','+correctedName+','+symbol+'\n')
		continue

	#other cases

	if (similarity!='0'):
		fileWrite.write(deepFashion+','+intu+',1\n')

fileRead.close()
fileWrite.close()