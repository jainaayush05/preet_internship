import os
import shutil
import math

trainSplit = 0.7
testSplit = 0.1
validationSplit = 0.2

sourceMain = './img'
files  = os.listdir(sourceMain)

# for file in files:
# 	print file

os.mkdir('imgTrain')
os.mkdir('imgValidation')
os.mkdir('imgTest')

os.chdir('./imgTrain')
for file in files:
	os.mkdir(file)

os.chdir('./../imgValidation')
for file in files:
	os.mkdir(file)

os.chdir('./../imgTest')
for file in files:
	os.mkdir(file)

os.chdir('./..') # now in PrelimTesting

for file in files:

	sourceFolder = './img/'+file

	trainDestination = './imgTrain/'+file
	validationDestination = './imgValidation/'+file
	testDestination = './imgTest/'+file

	try:
		images = os.listdir(sourceFolder)
	except:
		print sourceFolder
		continue
	l = len(images)

	nValidation = math.floor(l*validationSplit)
	nTest = math.floor(l*testSplit)
	nTrain = l - nTest - nValidation

	for image in images:
		if (nTrain != 0):
			nTrain = nTrain - 1
			shutil.move(sourceFolder+'/'+image,trainDestination+'/'+image)
			continue
		if (nValidation != 0):
			nValidation = nValidation - 1
			shutil.move(sourceFolder+'/'+image,validationDestination+'/'+image)
			continue
		if (nTest != 0):
			nTest = nTest - 1
			shutil.move(sourceFolder+'/'+image,testDestination+'/'+image)
			continue