# test the output files from datagen

# 2 hypothesis -- 1. all files intake and output in alphabetical order, 2. after data is generated once, the next flow has data right from the beginning

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras.applications.inception_v3 import InceptionV3
import os

# dimensions of our images.
img_width, img_height = 150, 150
batch_size = 64

train_data_dir = './../../../../notPushOnGit/imgTrain'

datagen = ImageDataGenerator(rescale=1. / 255)

generator = datagen.flow_from_directory(
	train_data_dir,
	target_size=(img_width, img_height),
	batch_size=batch_size,
	class_mode=None,
	shuffle=False)

print len(generator.filenames)

# for i in range(len(generator.filenames)):
# 	print generator.filenames[i]

x = generator.filenames
x.sort()

for i in range(len(x)):
	if (x[i]!=generator.filenames[i]):
		print "NO1!!"

filesArray = []
folders = os.listdir(train_data_dir)
folders.sort()
for folder in folders:
	path = train_data_dir + '/' + folder
	files = os.listdir(path)
	files.sort()
	for i in range(0,len(files)):
		filesArray.append(folder+'/'+files[i])

print len(filesArray)

for i in range(len(generator.filenames)):
	if (generator.filenames[i]!=filesArray[i]):
		print "NO"
		print generator.filenames[i]
		print filesArray[i]