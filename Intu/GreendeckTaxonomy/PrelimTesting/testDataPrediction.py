import pandas
import os
import numpy as np
from comet_ml import Experiment
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras.applications.inception_v3 import InceptionV3

# Add the following code anywhere in your machine learning file
experiment = Experiment(api_key="I8OFNMB0fwfHUA5wTPF9lX2TB")

# creating labels for the data

#arrange all classes in asc order, maintain that order

l=[] #list of all classes in asc alphabetical order
file = open('./L2.csv','r')
for line in file:
	line=line.split(',')[0].strip()
	l.append(line)
file.close()
file = open('./L3.csv','r')
for line in file:
	line=line.split(',')[0].strip()
	l.append(line)
file.close()
file = open('./L4.csv','r')
for line in file:
	line=line.split(',')[0].strip()
	l.append(line)
file.close()

l.sort()
#adding a "NONE" class at the end
l.append("ZZZZ")

# file=open('classIndexing.csv','w')
# for i in range(0,len(l)):
# 	file.write(l[i]+',')
# file.close()

class2index = dict() # mapping a class to it's index
for i in range(0,len(l)):
	class2index[l[i]] = i

# mapping DF folder to a one- hot vector
dfFolder2vector = dict()

df = pandas.read_csv('./gdTaxonomyProcessed.csv')
deepFashion = df['Deep Fashion']
l2 = df['L2']
l3 = df['L3']
l4 = df['L4']

for i in range(0,len(deepFashion)):
	#every folder name that can be mapped through the excel is alotted a vector
	folderName = deepFashion[i].strip()
	arr = [0]*len(l)
	l2str = l2[i].strip()
	l3str = l3[i].strip()
	l4str = l4[i].strip()
	marked = 0
	try:
		if (len(l2str)>1):
			arr[class2index[l2str]] = 1
			marked = 1
		if (len(l3str)>1):
			arr[class2index[l3str]] = 1
			marked = 1
		if (len(l4str)>1):
			arr[class2index[l4str]] = 1
			marked = 1
		if (marked == 0):
			arr[class2index["ZZZZ"]] = 1
		dfFolder2vector[folderName] = arr
	except:
		print (folderName)
		continue

# for a folder, datagen loads files in alphabetical order...

# label files for folders in alphabetical order:-
# y_train = []
# y_validation = []
y_test = []

sourceTrain = './imgTrain'
# sourceValidation = './imgValidation'
sourceTest = './imgTest'

filesTrain = os.listdir(sourceTrain)

filesTrain.sort()

for i in range(0,len(filesTrain)):

	folderName = filesTrain[i].strip()
	try:
		hotVector = dfFolder2vector[folderName]
	except:
		# that folder not mapped into the taxonomy we are using
		if (folderName == ".7z"):
			print (folderName)
			continue
		hotVector = [0]*len(l)
		hotVector[len(l)-1] = 1
		# hotVector = [0]*len(l) #all zeros

		# print folderName
		# continue

	# sourceTrainFolder = sourceTrain + '/' + folderName
	# sourceValidationFolder = sourceValidation + '/' + folderName
	sourceTestFolder = sourceTest + '/' + folderName

	# sourceTrainFolderImages = os.listdir(sourceTrainFolder)
	# sourceValidationFolderImages = os.listdir(sourceValidationFolder)
	sourceTestFolderImages = os.listdir(sourceTestFolder)

	# for j in range(0,len(sourceTrainFolderImages)):
	# 	y_train.append(hotVector)
	# for j in range(0,len(sourceValidationFolderImages)):
	# 	y_validation.append(hotVector)
	for j in range(0,len(sourceTestFolderImages)):
		y_test.append(hotVector)

print (len(y_test))
print (len(y_test[0]))

img_width, img_height = 150, 150

test_data_dir = 'imgTest'

nb_test_samples = len(y_test)

batch_size = 32
y_test = y_test[0:batch_size*int(len(y_test)/batch_size)]

print (len(y_test))
print (len(y_test[0]))

top_model = load_model('bottleneck_fc_model.h5')
model = InceptionV3(weights='imagenet', include_top=False, input_shape=(150,150,3))
# add the model on top of the convolutional base
# model.add(top_model)
model = Model(inputs=model.input, outputs=top_model(model.output))

datagen = ImageDataGenerator(rescale=1. / 255)

