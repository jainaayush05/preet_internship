import pandas
import os
import numpy as np
from comet_ml import Experiment
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras.applications.inception_v3 import InceptionV3

# Add the following code anywhere in your machine learning file
experiment = Experiment(api_key="I8OFNMB0fwfHUA5wTPF9lX2TB")

# creating labels for the data

#arrange all classes in asc order, maintain that order

l=[] #list of all classes in asc alphabetical order
# file = open('./L2.csv','r')
# for line in file:
# 	line=line.split(',')[0].strip()
# 	l.append(line)
# file.close()
file = open('./L3.csv','r')
for line in file:
	line=line.split(',')[0].strip()
	l.append(line)
file.close()
# file = open('./L4.csv','r')
# for line in file:
# 	line=line.split(',')[0].strip()
# 	l.append(line)
# file.close()

l.sort()
#adding a "NONE" class at the end
l.append("ZZZZ")

file=open('classIndexing.csv','w')
for i in range(0,len(l)):
	file.write(l[i]+',')
file.close()

class2index = dict() # mapping a class to it's index
for i in range(0,len(l)):
	class2index[l[i]] = i

# mapping DF folder to a one- hot vector
dfFolder2vector = dict()

df = pandas.read_csv('./gdTaxonomyProcessed.csv')
deepFashion = df['Deep Fashion']
# l2 = df['L2']
l3 = df['L3']
# l4 = df['L4']

for i in range(0,len(deepFashion)):
	#every folder name that can be mapped through the excel is alotted a vector
	folderName = deepFashion[i].strip()
	arr = [0]*len(l)
	# l2str = l2[i].strip()
	l3str = l3[i].strip()
	# l4str = l4[i].strip()
	marked = 0
	try:
		# if (len(l2str)>1):
		# 	arr[class2index[l2str]] = 1
		# 	marked = 1
		if (len(l3str)>1):
			arr[class2index[l3str]] = 1
			marked = 1
		# if (len(l4str)>1):
		# 	arr[class2index[l4str]] = 1
		# 	marked = 1
		if (marked == 0):
			arr[class2index["ZZZZ"]] = 1
		dfFolder2vector[folderName] = arr
	except:
		print (folderName)
		continue

# for a folder, datagen loads files in alphabetical order...

# label files for folders in alphabetical order:-
y_train = []
y_validation = []
# y_test = []

sourceTrain = './imgTrain'
sourceValidation = './imgValidation'
# sourceTest = './imgTest'

filesTrain = os.listdir(sourceTrain)

filesTrain.sort()

for i in range(0,len(filesTrain)):

	folderName = filesTrain[i].strip()
	try:
		hotVector = dfFolder2vector[folderName]
	except:
		# that folder not mapped into the taxonomy we are using
		if (folderName == ".7z"):
			print (folderName)
			continue
		hotVector = [0]*len(l)
		hotVector[len(l)-1] = 1
		# hotVector = [0]*len(l) #all zeros

		# print folderName
		# continue

	sourceTrainFolder = sourceTrain + '/' + folderName
	sourceValidationFolder = sourceValidation + '/' + folderName
	# sourceTestFolder = sourceTest + '/' + folderName

	sourceTrainFolderImages = os.listdir(sourceTrainFolder)
	sourceValidationFolderImages = os.listdir(sourceValidationFolder)
	# sourceTestFolderImages = os.listdir(sourceTestFolder)

	for j in range(0,len(sourceTrainFolderImages)):
		y_train.append(hotVector)
	for j in range(0,len(sourceValidationFolderImages)):
		y_validation.append(hotVector)
	# for j in range(0,len(sourceTestFolderImages)):
		# y_test.append(hotVector)
print (len(y_train))
print (len(y_train[0]))
print (len(y_validation))
print (len(y_validation[0]))

#beginning training

# dimensions of our images.
img_width, img_height = 150, 150

top_model_weights_path = 'L3bottleneck_fc_model.h5'

train_data_dir  = 'imgTrain'
validation_data_dir = 'imgValidation'

nb_train_samples = len(y_train)
nb_validation_samples = len(y_validation)

epochs = 30
batch_size = 64

y_train = y_train[0:batch_size*int(len(y_train)/batch_size)]
y_validation = y_validation[0:batch_size*int(len(y_validation)/batch_size)]

print (len(y_train))
print (len(y_train[0]))
print (len(y_validation))
print (len(y_validation[0]))

def save_bottlebeck_features():
	datagen = ImageDataGenerator(rescale=1. / 255)
	model = InceptionV3(include_top=False, weights='imagenet')
	generator = datagen.flow_from_directory(
		train_data_dir,
		target_size=(img_width, img_height),
		batch_size=batch_size,
		class_mode=None,
		shuffle=False)
	bottleneck_features_train = model.predict_generator(generator, nb_train_samples // batch_size)
	np.save(open('L3bottleneck_features_train.npy', 'wb'),
			bottleneck_features_train)

	generator = datagen.flow_from_directory(
		validation_data_dir,
		target_size=(img_width, img_height),
		batch_size=batch_size,
		class_mode=None,
		shuffle=False)
	bottleneck_features_validation = model.predict_generator(
		generator, nb_validation_samples // batch_size)
	np.save(open('L3bottleneck_features_validation.npy', 'wb'),
			bottleneck_features_validation)

def train_top_model():
	train_data = np.load(open('L3bottleneck_features_train.npy','rb'))
	train_labels = np.array(y_train)
	print (train_data.shape)
	print (train_labels.shape)

	validation_data = np.load(open('L3bottleneck_features_validation.npy','rb'))
	validation_labels = np.array(y_validation)
	print (validation_data.shape)
	print (validation_labels.shape)

	model = Sequential()
	model.add(Flatten(input_shape=train_data.shape[1:]))
	model.add(Dense(256, activation='relu'))
	model.add(Dropout(0.5))
	model.add(Dense(len(y_train[0]), activation='sigmoid'))

	model.compile(optimizer='adam',
				  loss='binary_crossentropy', metrics=['accuracy','categorical_accuracy'])

	model.fit(train_data, train_labels,
			  epochs=epochs,
			  batch_size=batch_size,
			  validation_data=(validation_data, validation_labels))
	model.save(top_model_weights_path)


save_bottlebeck_features()
train_top_model()