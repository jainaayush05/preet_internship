import pandas
import os
import numpy as np
from comet_ml import Experiment
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras.applications.inception_v3 import InceptionV3

Add the following code anywhere in your machine learning file
experiment = Experiment(api_key="I8OFNMB0fwfHUA5wTPF9lX2TB")

# Run your code and go to https://www.comet.ml

# creating labels for the data

#arrange all classes in asc order, maintain that order

l=[] #list of all classes in asc alphabetical order
file = open('./../DataCleaning/L2.csv','r')
for line in file:
	line=line.split(',')[0].strip()
	l.append(line)
file.close()
file = open('./../DataCleaning/L3.csv','r')
for line in file:
	line=line.split(',')[0].strip()
	l.append(line)
file.close()
file = open('./../DataCleaning/./../DataCleaning/L4.csv','r')
for line in file:
	line=line.split(',')[0].strip()
	l.append(line)
file.close()

l.sort()

file=open('classIndexing.csv','w')
for i in range(0,len(l)):
	file.write(l[i]+',')
file.close()

class2index = dict() # mapping a class to it's index
for i in range(0,len(l)):
	class2index[l[i]] = i

# mapping DF folder to a one- hot vector
dfFolder2vector = dict()

df = pandas.read_csv('./../DataCleaning/gdTaxonomyProcessed.csv')
deepFashion = df['Deep Fashion']
l2 = df['L2']
l3 = df['L3']
l4 = df['L4']

for i in range(0,len(deepFashion)):
	folderName = deepFashion[i].strip()
	arr = [0]*len(l)
	l2str = l2[i].strip()
	l3str = l3[i].strip()
	l4str = l4[i].strip()
	try:
		if (len(l2str)>1):
			arr[class2index[l2str]] = 1
		if (len(l3str)>1):
			arr[class2index[l3str]] = 1
		if (len(l4str)>1):
			arr[class2index[l4str]] = 1
		dfFolder2vector[folderName] = arr
	except:
		print l2str
		print l3str
		print l4str
		continue

# for a folder, datagen loads files in alphabetical order...

# label files for folders in alphabetical order:-
y_train = []
y_validation = []
# y_test = []

sourceTrain = './imgTrain'
sourceValidation = './imgValidation'
# sourceTest = './imgTest'

filesTrain = os.listdir(sourceTrain)

filesTrain.sort()

for i in range(0,len(filesTrain)):

	folderName = filesTrain[i].strip()
	try:
		hotVector = dfFolder2vector[folderName]
	except:
		# that folder not mapped into the taxonomy we are using
		if (folderName == ".7z"):
			print (folderName)
			continue
		hotVector = [0]*len(l) #all zeros

		# print folderName
		# continue

	sourceTrainFolder = sourceTrain + '/' + folderName
	sourceValidationFolder = sourceValidation + '/' + folderName
	# sourceTestFolder = sourceTest + '/' + folderName

	sourceTrainFolderImages = os.listdir(sourceTrainFolder)
	sourceValidationFolderImages = os.listdir(sourceValidationFolder)
	# sourceTestFolderImages = os.listdir(sourceTestFolder)

	for j in range(0,len(sourceTrainFolderImages)):
		y_train.append(hotVector)
	for j in range(0,len(sourceValidationFolderImages)):
		y_validation.append(hotVector)
	# for j in range(0,len(sourceTestFolderImages)):
		# y_test.append(hotVector)

#beginning training

# dimensions of our images.

img_width = 150 
img_height = 150, 150

top_model_weights_path = 'bottleneck_fc_model.h5'

train_data_dir  = 'imgTrain'
validation_data_dir = 'imgValidation'

nb_train_samples = 166161
nb_validation_samples = 44591

epochs = 50
batch_size = 64

def save_bottlebeck_features():
	datagen = ImageDataGenerator(rescale=1. / 255)
	model = InceptionV3(include_top=False, weights='imagenet')
	generator = datagen.flow_from_directory(
		train_data_dir,
		target_size=(img_width, img_height),
		batch_size=batch_size,
		class_mode=None,
		shuffle=False)
	bottleneck_features_train = model.predict_generator(
        generator, nb_train_samples // batch_size)
	np.save(open('bottleneck_features_train.npy', 'w'),
			bottleneck_features_train)

	generator = datagen.flow_from_directory(
		validation_data_dir,
		target_size=(img_width, img_height),
		batch_size=batch_size,
		class_mode=None,
		shuffle=False)
	bottleneck_features_validation = model.predict_generator(
		generator, nb_validation_samples // batch_size)
	np.save(open('bottleneck_features_validation.npy', 'w'),
			bottleneck_features_validation)

def train_top_model():
	train_data = np.load(open('bottleneck_features_train.npy'))
	train_labels = np.array(y_train)

	validation_data = np.load(open('bottleneck_features_validation.npy'))
	validation_labels = np.array(y_validation)

	model = Sequential()
	model.add(Flatten(input_shape=train_data.shape[1:]))
	model.add(Dense(256, activation='relu'))
	model.add(Dropout(0.5))
	model.add(Dense(len(y_train[0]), activation='sigmoid'))

	model.compile(optimizer='adam',
				  loss='binary_crossentropy', metrics=['accuracy','categorical_accuracy'])

	model.fit(train_data, train_labels,
			  epochs=epochs,
			  batch_size=batch_size,
			  validation_data=(validation_data, validation_labels))
	model.save_weights(top_model_weights_path)


save_bottlebeck_features()
train_top_model()