import pandas
import numpy as np
import os

d = dict() #mapping subfolde name of deepfashion to the number of images inside it
# fileRead = open('list_category_img.txt','r')
# test=0
# for line in fileRead:
# 	if (test<2):
# 		test=test+1
# 		continue
# 	# x = fileRead.readline()
# 	x = line.split('/')
# 	if (len(x)>3):
# 		print line
# 	# print x[1]
# 	className = x[1].strip()
# 	if (className in d):
# 		d[className]+=1
# 	else:
# 		d[className]=1
# fileRead.close()

folder_path = './../PrelimTesting/imgTrain'
subfolders = os.listdir(folder_path)

for subfolder in subfolders:
	subfolder = subfolder.strip()
	# if (subfolder == '.7z'):
	# 	continue	
	path = folder_path+'/'+subfolder
	files = os.listdir(path)
	d[subfolder] = len(files)
	# if (subfolder == 'Boxy_Ombr&eacute'):
	# 	print "yayy"

df = pandas.read_csv('gdTaxonomyProcessed.csv')

columns = ['L2','L3','L4']
deepFashion = df['Deep Fashion']

for column in columns:

	thisColumn = df[column]
	labelMap = dict()
	for i in range(0,len(thisColumn)):
		try:
			label = thisColumn[i].strip()
			if (len(label)==1):
				continue
			dfClass = deepFashion[i].strip()
			# print label
			try:
				if (label in labelMap):
					labelMap[label]+=d[dfClass]
				else:
					labelMap[label]=d[dfClass]
			except:
				print dfClass
		except:
			#NAN
			continue

	file = open(column+'.csv', 'w')
	for label in labelMap:
		file.write(label+" , "+str(labelMap[label])+'\n')
	file.close()