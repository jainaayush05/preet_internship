import pandas

df = pandas.read_csv('deepfashion_classes.csv')
gd = df['Greendeck']
# print len(gd)
# print type(gd)
l1=[]
l2=[]
l3=[]
l4=[]

for i in range(0,len(gd)):

	thisGd = gd[i]
	thisGd = thisGd[1:len(thisGd)-1]

	arr = thisGd.split(',')
	l1.append(((arr[0]).split("'"))[3])
	l2.append(((arr[1]).split("'"))[3])
	l3.append(((arr[2]).split("'"))[3])
	l4.append(((arr[3]).split("'"))[3])

df['L1'] = l1
df['L2'] = l2
df['L3'] = l3
df['L4'] = l4

df.to_csv('gdTaxonomyProcessed.csv', encoding='utf-8', index=False)
del df