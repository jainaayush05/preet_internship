# Purpose: download images of iMaterial-Fashion dataset

# Images that already exist will not be downloaded again, so the script can
# resume a partially completed download. All images will be saved in the JPG
# format with 90% compression quality.

######################################################################################################################
## Imports
######################################################################################################################



import sys, os, multiprocessing, urllib3, csv
from PIL import Image
from io import BytesIO
from tqdm  import tqdm
import json
import pandas

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

######################################################################################################################
## Functions
######################################################################################################################

client = urllib3.PoolManager(500)

def ParseData(data_file):

  key_url_list = []

  add_list = []
  url_list = []

  for chunk in pandas.read_csv(data_file,chunksize=20000):

    add_chunk = (chunk.iloc[:,0])
    for element in add_chunk:
      add_list.append(element)

    url_chunk = (chunk.iloc[:,1])
    for element in url_chunk:
      url_list.append(element)

  print ("both lengths should be equal..")
  print (len((add_list)))
  print (len(url_list))

  for i in range(len(add_list)):
    key_url_list.append((add_list[i],url_list[i]))

  return key_url_list


requirement = {"Hair":3000, "Purses":3000,"Umbrellas":2300,"Briefcases":1200,"Clutch":3000, "Gym & Sports":3000, "Luggage":3000,"Messenger":2200,"Shoulder Bags":3000,"Tote Bags":2200,"Veils & Headdresses":3000,"Flats":3000,"Heels":3000,"Wellies":3000}

def DownloadImage(key_url):

  out_dir = sys.argv[2]
  (key, url) = key_url
  filename = os.path.join(out_dir, '%s.jpg' % key)

  paths = key.split("/")
  subdirectory = ""
  for i,p in enumerate(paths):
      if i != len(paths) - 1:
          subdirectory = subdirectory + "/" + p
  subdirectory = out_dir + subdirectory

  if (len(paths) <= 2):
  	print ('len < 2')
  	return

  if (paths[1] not in requirement):
  	print (key + ' not required')
  	return

  if (requirement[paths[1]] > 0):
  	requirement[paths[1]]-=1
  else:
  	print (key+' fulfiled already')
  	return

  if not os.path.exists(subdirectory):
        os.makedirs(subdirectory)

  if os.path.exists(filename):
    print('Image %s already exists. Skipping download.' % filename)
    return

  try:
    global client
    response = client.request('GET', url)#, timeout=30)
    image_data = response.data
  except:
    print('Warning: Could not download image %s from %s' % (key, url))
    return

  try:
    pil_image = Image.open(BytesIO(image_data))
  except:
    print('Warning: Failed to parse image %s %s' % (key,url))
    return

  try:
  	pil_image = pil_image.resize((400, 400),Image.ANTIALIAS)
  except:
    print('Warning: Failed to resize image %s %s' % (key,url))
    return

  try:
    pil_image_rgb = pil_image.convert('RGB')
  except:
    print('Warning: Failed to convert image %s to RGB' % key)
    return

  try:
    pil_image_rgb.save(filename, format='JPEG', quality=90)
  except:
    print('Warning: Failed to save image %s' % filename)
    return


def Run():

  if len(sys.argv) != 3:
    print('Syntax: %s <train|validation|test.json> <output_dir/>' % sys.argv[0])
    sys.exit(0)
  (data_file, out_dir) = sys.argv[1:]

  if not os.path.exists(out_dir):
    os.mkdir(out_dir)

  key_url_list = ParseData(data_file)
  pool = multiprocessing.Pool(processes=30)

  with tqdm(total=len(key_url_list)) as bar:
    for _ in pool.imap_unordered(DownloadImage, key_url_list):
      bar.update(1)


if __name__ == '__main__':
  Run()
