import os

def find_distribution(source):
	class_freq = dict()
	folders = os.listdir(source)
	for folder in folders:
		images = os.listdir(source+'/'+folder)
		class_freq[folder] = len(images)
	return class_freq

def save_file(class_freq, out_filename):
	file = open(out_filename, 'w')
	for c in class_freq:
		file.write(c + ',' + str(class_freq[c]) + '\n')
	file.close()

def run(source, out_filename):
	class_freq = find_distribution(source)
	save_file(class_freq, out_filename)

