import os

def run(path):

	# give freq of all subfolders in the path
	# assuming path given for data_downloaded folder

	files = os.listdir(path)
	num = 0
	subfolders = []
	for file in files:
		if ('.jpg' not in file):
			subfolders.append(file)
		else:
			num+=1
	print (path + ", " + num)
	for subfolder in subfolders:
		run(path+'/'+subfolder)

run(data_downloaded)