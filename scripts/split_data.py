"""
split L1_Data into train, validation and test
"""

import os
import random
import shutil

def make_directory(name):
	try:
		os.mkdir(name)
	except:
		print ("didn't make directory "+name)
		pass

def split(source, train_destination, validation_destination, test_destination, train_split, validation_split, test_split, constraint_dict):

	"""
	constraint_dict ==> class->(total, to be taken); splits in percentage
	"""
	l1_classes = os.listdir(source)

	make_directory(train_destination)
	make_directory(validation_destination)
	make_directory(test_destination)

	for l1_class in l1_classes:

		#making that class folder in train, test and val. folders

		make_directory(train_destination+'/'+l1_class)
		make_directory(validation_destination+'/'+l1_class)
		make_directory(test_destination+'/'+l1_class)

		selection_percentage = 100*((constraint_dict[l1_class])[1])/(constraint_dict[l1_class])[0] # accounting for class imbalance

		all_images = os.listdir(source+'/'+l1_class)

		# randomly selecting images according to selection_fraction

		selected_images = []

		for i in range(len(all_images)):
			random_number = random.randint(1,101) #between 1 and 100, both inclusive
			if (random_number <= selection_percentage):
				selected_images.append(all_images[i])


		for i in range(len(selected_images)):
			random_number = random.randint(1,101)
			if (random_number <= train_split):
				# move to train folder
				shutil.copy(source+'/'+l1_class+'/'+selected_images[i], train_destination+'/'l1_class)

			elif (random_number <= train_split + validation_split):
				shutil.copy(source+'/'+l1_class+'/'+selected_images[i], validation_destination+'/'l1_class)

			else:
				shutil.copy(source+'/'+l1_class+'/'+selected_images[i], test_destination+'/'l1_class)





