import os
import shutil

def make_directory(destination_dir):
	try:
		os.mkdir(destination_dir)
	except:
		pass

def copy_all(source_dir, destination_dir):

	images = os.listdir(source_dir)
	subfolders = []
	for image in images:
		if ('.jpg' in image):
			# copy
			shutil.copy(source_dir+'/'+image, destination_dir)
		else:
			subfolders.append(image)

	for subfolder in subfolders:
		copy_all(source_dir+'/'+subfolder, destination_dir)

def run(source_dir, destination_dir):
	"""
	constraint dict ==> L1 class -> (images to be selected, total images)
	"""
	make_directory(destination_dir)
	l1_classes = os.listdir(source_dir)

	os.chdir(destination_dir)
	for l1_class in l1_classes:

		#creating similar folder in the destination
		
		make_directory(l1_class)
	os.chdir('./..')

	for l1_class in l1_classes:
		copy_all(source_dir+'/'+l1_class, destination_dir+'/'+l1_class)


source_dir = "data_download" #directory in which all the images are downloaded
destination_dir = "L1_Data" #directory where all the data would be stored

run(source_dir, destination_dir)