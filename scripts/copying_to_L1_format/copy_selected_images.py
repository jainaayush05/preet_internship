import pandas
import shutil
import random
import os

def make_directory(destination_dir):
	try:
		os.mkdir(destination_dir)
	except:
		pass

def copy(source_path, train_path, validation_path, test_path, ratio, train_percentage, validation_percentage, test_percentage):

	all_images = os.listdir(source_path)
	train_percentage *= ratio
	validation_percentage *= ratio
	test_percentage *= ratio

	for image in all_images:
		if ('.jpg' not in image):
			# accounting for the folders
			continue
		random_number = random.randint(1,101)
		if (random_number <= train_percentage):
			shutil.copy(source_path+'/'+image, train_path)
		elif (random_number <= train_percentage+validation_percentage):
			shutil.copy(source_path+'/'+image, validation_path)
		elif (random_number <= train_percentage+validation_percentage+test_percentage):
			shutil.copy(source_path+'/'+image, test_path)
		else:
			continue

def run(path_file, destination_dir, data_source, train_percentage, validation_percentage, test_percentage):

	make_directory(destination_dir)
	make_directory(destination_dir+'/'+'train')
	make_directory(destination_dir+'/'+'validation')
	make_directory(destination_dir+'/'+'test')

	folder_list = os.listdir(data_source)
	for folder in folder_list:
		make_directory(destination_dir+'/'+'train'+'/'+folder)
		make_directory(destination_dir+'/'+'validation'+'/'+folder)
		make_directory(destination_dir+'/'+'test'+'/'+folder)

	df = pandas.read_csv(path_file)

	paths = df['Folder']
	total = df['Available']
	select = df['Final']

	for i in range(len(paths)):

		print ("now proccessing, ", paths[i])

		if (total[i] == 0 or select[i] == 0):
			continue

		ratio = select[i] / total[i]

		path_arr = paths[i].split('/')

		train_path = destination_dir + '/' + 'train' + '/' + path_arr[2]
		validation_path = destination_dir + '/' + 'validation' + '/' + path_arr[2]
		test_path = destination_dir + '/' + 'test' + '/' + path_arr[2]

		source_path = data_source

		for i in range(2,len(path_arr)):
			source_path = source_path + '/' + path_arr[i]

		copy(source_path, train_path, validation_path, test_path, ratio, train_percentage, validation_percentage, test_percentage)

run('selection_ratio.csv', "Intu1-Data", "./../data_download", 70, 20, 10)