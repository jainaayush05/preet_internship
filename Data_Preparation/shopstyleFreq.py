import pandas

def getFreq(arr):
	intu2freq = dict()
	for i in range(len(arr)):
		entry = arr[i]
		for j in range(len(entry)):
			this_entry = str(entry[j])
			# print this_entry
			if (this_entry in intu2freq):
				# print "increasing frequency"
				intu2freq[this_entry]+=1
			else:
				# print "introducing new"
				intu2freq[this_entry]=1
	return intu2freq

def saveFile(category, intu2freq):
	file = open(category+'_shopstyle_frequency.csv','w')
	for x in intu2freq:
		file.write(x+','+str(intu2freq[x])+'\n')
	file.close()

def run(category_array, file_name):
	mylist = []

	# for chunk in  pandas.read_csv('shopstyle.csv',chunksize=20000):
	#     mylist.append(chunk)

	# df = pandas.concat(mylist, axis= 0)
	# print len(df)
	# del mylist
	# for category in category_array:
	# 	intu2freq = getFreq(category, df)
	# 	saveFile(category, intu2freq)

	for category in category_array:
		print (category)
		arr= []
		for chunk in  pandas.read_csv('shopstyle.csv',chunksize=20000):
			# print type(chunk.iloc[:,16])
			x = (chunk.iloc[:,category])
			for element in x:
				arr.append(x)
		# print len(arr)
		intu2freq = getFreq(arr)
		saveFile(category, intu2freq)
		# for i in range(0,len(arr)):
		# 	print [i]
		# 	print ".........."
		# 	print len(arr[i])
		# 	print ".........."
		# 	print arr[i][1]
		# 	print ".........."
		# 	print arr[i][2]
		# 	print ".........."
		# 	print arr[i][3]
		# 	break

run ([16,17,18], 'shopstyle.csv')