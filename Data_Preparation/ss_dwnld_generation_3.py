"""
Specific conditions over cells
"""

import pandas
addresses=[]
urls=[]
intu1=[]
intu2=[]
intu3=[]
ss_id=[]
ss2=[]
ss3=[]
ss4=[]
best_image_url=[]
image_url0=[]
image_url1=[]
image_url2=[]
image_url3=[]
image_url4=[]
image_url5=[]
for chunk in pandas.read_csv('./shopstyle.csv',chunksize=20000):

	intu2_chunk = (chunk.iloc[:,17])
	for element in intu2_chunk:
		intu2.append(element)
		
	intu1_chunk = (chunk.iloc[:,16])
	for element in intu1_chunk:
		# print (type(element)) -- str
		# print (element)
		# print (".......")
		# print (type(element[0]))
		# print (element[0])
		# print (".......")
		# print (type(element[0][0]))
		# print (element[0][0])
		intu1.append(element)

	

	intu3_chunk = (chunk.iloc[:,18])
	for element in intu3_chunk:
		intu3.append(element)

	ss2_chunk = (chunk.iloc[:,12])
	for element in ss2_chunk:
		ss2.append(element)

	ss3_chunk = (chunk.iloc[:,13])
	for element in ss3_chunk:
		ss3.append(element)

	ss4_chunk = (chunk.iloc[:,14])
	for element in ss4_chunk:
		ss4.append(element)
	best_image_url_chunk = (chunk.iloc[:,4])
	for element in best_image_url_chunk:
		best_image_url.append(element)

	image_url0_chunk = (chunk.iloc[:,5])
	for element in image_url0_chunk:
		image_url0.append(element)

	image_url1_chunk = (chunk.iloc[:,6])
	for element in image_url1_chunk:
		image_url1.append(element)

	image_url2_chunk = (chunk.iloc[:,7])
	for element in image_url2_chunk:
		image_url2.append(element)

	image_url3_chunk = (chunk.iloc[:,8])
	for element in image_url3_chunk:
		image_url3.append(element)

	image_url4_chunk = (chunk.iloc[:,9])
	for element in image_url4_chunk:
		image_url4.append(element)

	image_url5_chunk = (chunk.iloc[:,10])
	for element in image_url5_chunk:
		image_url5.append(element)

	ss_id_chunk = (chunk.iloc[:,1])
	for element in ss_id_chunk:
		ss_id.append(element)

count1 = 3000
count2 = 3000
count3 = 3000
count4 = 3000
count5 = 3300

for i in range(len(intu1)):
	this_intu2 = intu2[i]
	this_intu1 = intu1[i]
	this_intu3 = intu3[i]
	this_best_image_url = best_image_url[i]
	this_image_url0 = image_url0[i]
	this_image_url1 = image_url1[i]
	this_image_url2 = image_url2[i]
	this_image_url3 = image_url3[i]
	this_image_url4 = image_url4[i]
	this_image_url5 = image_url5[i]
	this_ss_id = str(ss_id[i])
	this_ss2 = ss2[i]
	this_ss3 = ss3[i]
	this_ss4 = ss4[i]

	this_address = ""

	# clutch bags, vests, purses, luggage, hair, bikinis, 

	# hair

	# try:


	# 	if ('hair-accessories' in this_ss3 and count1>0):
	# 		print ('hair found!')
	# 		this_address = "Accessories/Hair"
	# 		count1-=1

	# except:
	# 	pass

	# try:

	# 	if (this_intu2 == "Clutch Bags" and count2>0 and this_address==""):
	# 		print ('clutch bag found!')
	# 		this_address = "Bags/Clutch Bags"
	# 		count2-=1
	# except:
	# 	pass

	# try:

	# 	if ('luggage' in this_ss2 and count3>0 and this_address==""):
	# 		print ('luggage found!')
	# 		this_address = "Bags/Luggage"
	# 		count3-=1
	# except:
	# 	pass

	try:

		if ('evening-handbags' in this_ss3 and count3>0 and this_address==""):
			print ('purse found!')
			this_address = "Accessories/Purses"
			count3-=1
	except:
		pass



	# try:

	# 	if (this_intu3 == "Bikinis" and count4>0 and this_address==""):
	# 		print ('bikini found')
	# 		this_address = "Clothing/Swimwear/Bikinis"
	# 		count4-=1
	# except:
	# 	pass

	# try:

	# 	if (this_intu3 == "Vests" and count5>0 and this_address==""):
	# 		print ('vest found')
	# 		this_address  = "Clothing/Knitwear/Vests"
	# 		count5-=1
	# except:
	# 	pass

	if (this_address==""):
		continue

	try:
		this_best_image_url = this_best_image_url.strip()
		# u = this_best_image_url.split('/')[len(this_best_image_url.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#best')

		# insert limit condition here..

		urls.append(this_best_image_url)
	except:
		pass

	try:
		this_image_url0 = this_image_url0.strip()
		# u = this_image_url0.split('/')[len(this_image_url0.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#0')
		urls.append(this_image_url0)
	except:
		pass

	try:
		this_image_url1 = this_image_url1.strip()
		# u = this_image_url1.split('/')[len(this_image_url1.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#1')
		urls.append(this_image_url1)
	except:
		pass

	try:
		this_image_url2 = this_image_url2.strip()
		# u = this_image_url2.split('/')[len(this_image_url2.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#2')
		urls.append(this_image_url2)
	except:
		pass

	try:
		this_image_url3 = this_image_url3.strip()
		# u = this_image_url3.split('/')[len(this_image_url3.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#3')
		urls.append(this_image_url3)
	except:
		pass

	try:
		this_image_url4 = this_image_url4.strip()
		# u = this_image_url4.split('/')[len(this_image_url4.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#4')
		urls.append(this_image_url4)
	except:
		pass

	try:
		this_image_url5 = this_image_url5.strip()
		# u = this_image_url5.split('/')[len(this_image_url5.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#5')
		urls.append(this_image_url5)
	except:
		pass

file = open('url_download_helper_3.csv','w')
for i in range(len(addresses)):
	file.write(addresses[i]+','+urls[i]+'\n')
file.close()
