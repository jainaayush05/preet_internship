import pandas

intu1=[]
intu2=[]
intu3=[]
best_image_url=[]
image_url0=[]
image_url1=[]
image_url2=[]
image_url3=[]
image_url4=[]
image_url5=[]
ss_id = []

addresses = []
urls = []
# required = ["Umbrellas","Belts & Braces","Hair","Purses","Ties & Bow Ties","Clutch","Luggage","Gym & Sports","Shoulder Bags","Suits","Swimsuits","Veils & Headdresses","Boots","Flats","Heels","Lace-ups","Sandals","Trainers","Wedges","Wellies","Charms","Necklaces","Rings"]
not_required = ['Tops','Denim','Trousers','Swimwear','Lingerie']
max_required = 70000
required = dict()

for chunk in pandas.read_csv('./shopstyle.csv',chunksize=20000):
	is_exit = False

	intu2_chunk = (chunk.iloc[:,17])
	for element in intu2_chunk:
		intu2.append(element)
		
	intu1_chunk = (chunk.iloc[:,16])
	for element in intu1_chunk:
		# print (type(element)) -- str
		# print (element)
		# print (".......")
		# print (type(element[0]))
		# print (element[0])
		# print (".......")
		# print (type(element[0][0]))
		# print (element[0][0])
		intu1.append(element)

	

	intu3_chunk = (chunk.iloc[:,18])
	for element in intu3_chunk:
		intu3.append(element)

	best_image_url_chunk = (chunk.iloc[:,4])
	for element in best_image_url_chunk:
		best_image_url.append(element)

	image_url0_chunk = (chunk.iloc[:,5])
	for element in image_url0_chunk:
		image_url0.append(element)

	image_url1_chunk = (chunk.iloc[:,6])
	for element in image_url1_chunk:
		image_url1.append(element)

	image_url2_chunk = (chunk.iloc[:,7])
	for element in image_url2_chunk:
		image_url2.append(element)

	image_url3_chunk = (chunk.iloc[:,8])
	for element in image_url3_chunk:
		image_url3.append(element)

	image_url4_chunk = (chunk.iloc[:,9])
	for element in image_url4_chunk:
		image_url4.append(element)

	image_url5_chunk = (chunk.iloc[:,10])
	for element in image_url5_chunk:
		image_url5.append(element)

	ss_id_chunk = (chunk.iloc[:,1])
	for element in ss_id_chunk:
		ss_id.append(element)

# print intu2[450]

# limit = 20000 # max number of intended images for processing


for i in range(len(intu1)):

	this_intu1 = intu1[i]
	if (this_intu1!="Clothing"):
		continue
	this_intu2 = intu2[i]
	if (this_intu2 in not_required):
		continue
	else:
		try:
			this_intu2 = this_intu2.strip()
		except:
			continue
		if (len(this_intu2)==1):
			continue
		if (this_intu2 not in required):
			required[this_intu2] = max_required

	if (required[this_intu2] < 0):
		continue
	this_intu3 = intu3[i]
	this_best_image_url = best_image_url[i]
	this_image_url0 = image_url0[i]
	this_image_url1 = image_url1[i]
	this_image_url2 = image_url2[i]
	this_image_url3 = image_url3[i]
	this_image_url4 = image_url4[i]
	this_image_url5 = image_url5[i]
	this_ss_id = str(ss_id[i])

	this_address = ""
	is_finished = False

	#getting the first column

	try:
		this_intu1 = this_intu1.strip()
		if (len(this_intu1) > 1):
			this_address += this_intu1
		else:
			is_finished = True
	except:
		is_finished = True

	if (is_finished == False):
		try:
			this_intu2 = this_intu2.strip()
			if (len(this_intu2) > 1):
				this_address += '/' + this_intu2
			else:
				is_finished = True
		except:
			is_finished = True

	if (is_finished == False):
		try:
			this_intu3 = this_intu3.strip()
			if (len(this_intu3) > 1):
				this_address += '/' + this_intu3
			else:
				is_finished = True
		except:
			is_finished = True

	if (len(this_address) == 0):
		this_address = "No Label"

	try:
		this_best_image_url = this_best_image_url.strip()
		# u = this_best_image_url.split('/')[len(this_best_image_url.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#best')
		required[this_intu2]-=1

		# insert limit condition here..

		urls.append(this_best_image_url)
	except:
		pass

	try:
		this_image_url0 = this_image_url0.strip()
		# u = this_image_url0.split('/')[len(this_image_url0.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#0')
		urls.append(this_image_url0)
		required[this_intu2]-=1
	except:
		pass

	try:
		this_image_url1 = this_image_url1.strip()
		# u = this_image_url1.split('/')[len(this_image_url1.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#1')
		urls.append(this_image_url1)
		required[this_intu2]-=1
	except:
		pass

	try:
		this_image_url2 = this_image_url2.strip()
		# u = this_image_url2.split('/')[len(this_image_url2.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#2')
		urls.append(this_image_url2)
		required[this_intu2]-=1
	except:
		pass

	try:
		this_image_url3 = this_image_url3.strip()
		# u = this_image_url3.split('/')[len(this_image_url3.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#3')
		urls.append(this_image_url3)
		required[this_intu2]-=1
	except:
		pass

	try:
		this_image_url4 = this_image_url4.strip()
		# u = this_image_url4.split('/')[len(this_image_url4.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#4')
		urls.append(this_image_url4)
		required[this_intu2]-=1
	except:
		pass

	try:
		this_image_url5 = this_image_url5.strip()
		# u = this_image_url5.split('/')[len(this_image_url5.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id[0:len(this_ss_id)-2]+'#5')
		urls.append(this_image_url5)
		required[this_intu2]-=1
	except:
		pass

file = open('url_download_helper_2.csv','w')
for i in range(len(addresses)):
	file.write(addresses[i]+','+urls[i]+'\n')
file.close()

# print (len(intu1))
# print (len(intu2))
