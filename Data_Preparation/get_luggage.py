import pandas

addresses=[]
urls=[]

ss_id=[]

best_image_url=[]
image_url0=[]
image_url1=[]
image_url2=[]
image_url3=[]
image_url4=[]
image_url5=[]

for chunk in pandas.read_csv('./luggage.csv',chunksize=20000):

	ss_id_chunk = (chunk.iloc[:,0])
	for element in ss_id_chunk:
		ss_id.append(element)

	best_image_url_chunk = (chunk.iloc[:,4])
	for element in best_image_url_chunk:
		best_image_url.append(element)

	image_url0_chunk = (chunk.iloc[:,5])
	for element in image_url0_chunk:
		image_url0.append(element)

	image_url1_chunk = (chunk.iloc[:,6])
	for element in image_url1_chunk:
		image_url1.append(element)

	image_url2_chunk = (chunk.iloc[:,7])
	for element in image_url2_chunk:
		image_url2.append(element)

	image_url3_chunk = (chunk.iloc[:,8])
	for element in image_url3_chunk:
		image_url3.append(element)

	image_url4_chunk = (chunk.iloc[:,9])
	for element in image_url4_chunk:
		image_url4.append(element)

	image_url5_chunk = (chunk.iloc[:,10])
	for element in image_url5_chunk:
		image_url5.append(element)

count = 7000

for i in range(len(ss_id)):

	if (count<=0):
		break
	this_best_image_url = best_image_url[i]
	this_image_url0 = image_url0[i]
	this_image_url1 = image_url1[i]
	this_image_url2 = image_url2[i]
	this_image_url3 = image_url3[i]
	this_image_url4 = image_url4[i]
	this_image_url5 = image_url5[i]
	this_ss_id = str(ss_id[i])

	this_address = "Bags/Luggage"

	try:
		this_best_image_url = this_best_image_url.strip()
		# u = this_best_image_url.split('/')[len(this_best_image_url.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id+'#best')
		count = count-1;

		# insert limit condition here..

		urls.append(this_best_image_url)
	except:
		pass

	try:
		this_image_url0 = this_image_url0.strip()
		# u = this_image_url0.split('/')[len(this_image_url0.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id+'#0')
		urls.append(this_image_url0)
		count = count-1;
	except:
		pass

	try:
		this_image_url1 = this_image_url1.strip()
		# u = this_image_url1.split('/')[len(this_image_url1.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id+'#1')
		urls.append(this_image_url1)
		count = count-1;
	except:
		pass

	try:
		this_image_url2 = this_image_url2.strip()
		# u = this_image_url2.split('/')[len(this_image_url2.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id+'#2')
		urls.append(this_image_url2)
		count = count-1;
	except:
		pass

	try:
		this_image_url3 = this_image_url3.strip()
		# u = this_image_url3.split('/')[len(this_image_url3.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id+'#3')
		urls.append(this_image_url3)
		count = count-1;
	except:
		pass

	try:
		this_image_url4 = this_image_url4.strip()
		# u = this_image_url4.split('/')[len(this_image_url4.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id+'#4')
		urls.append(this_image_url4)
		count = count-1;
	except:
		pass

	try:
		this_image_url5 = this_image_url5.strip()
		# u = this_image_url5.split('/')[len(this_image_url5.split('/'))-1]
		# try:
		# 	u = u.split('_')[0]
		# except:
		# 	pass
		addresses.append(this_address+'/'+this_ss_id+'#5')
		urls.append(this_image_url5)
		count = count-1;
	except:
		pass

file = open('url_download_helper_luggage.csv','w')
for i in range(len(addresses)):
	file.write(addresses[i]+','+urls[i]+'\n')
file.close()
print count