from multiprocessing.dummy import Pool as ThreadPool
from googletrans import Translator
import json
import requests


class GoogleTranslator:
    def __init__(self, text_arr, mode="free", source=False, threads=50):
        self.text_arr = text_arr
        self.mode = mode
        self.threads= 50
        self.source = source
    def _google_api_translate(self,text):
        translated_text = ''
        key = 'AIzaSyCZEVZq9eFRSXjNURebnF6h1vVsCM8x5_8'
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+key
        }
        url = 'https://translation.googleapis.com/language/translate/v2'
        data = {
          'q': text,
          'target': 'en',
          'format': 'text'
        }
        if self.source:
            data['source'] = self.source
        res = json.loads(requests.post(url+'/?key='+key, json=data).text)
        translated_text = res['data']['translations'][0]['translatedText']
        return translated_text

    def _translate_one(self,text):
        if self.mode == "free":
            translator = Translator()
            translation = translator.translate(text, dest='en')
            return translation.text
        else:
            return self._google_api_translate(text)

    def translate(self):
        pool = ThreadPool(self.threads)
        results = pool.map(self._translate_one, self.text_arr)
        pool.close()
        pool.join()
        return results
