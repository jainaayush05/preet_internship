# Imports
from __future__ import division
import nltk.corpus
from nltk import word_tokenize
import nltk.stem.snowball
from nltk.corpus import wordnet
import string
from spiral import ronin
from nltk.corpus import wordnet as wn
from multiprocessing.dummy import Pool as ThreadPool
import sys
import csv
from sklearn.feature_extraction import text
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np


class TextSimilarity:

    def __init__(self,target="target.csv",dictionary="dictionary.csv",output="mapping.csv", threads=50):
        self.target_file = target
        self.dictionary_file = dictionary
        self.output_file = output
        self.threads = threads
        # Get default English stopwords and extend with punctuation
        self.stopwords = nltk.corpus.stopwords.words('english')
        self.stopwords.extend(string.punctuation)
        self.stopwords.append('')
        # Create tokenizer and stemmer
        self.lemmatizer = nltk.stem.wordnet.WordNetLemmatizer()


    def get_wordnet_pos(self, pos_tag):
        if pos_tag[1].startswith('J'):
            return (pos_tag[0], wordnet.ADJ)
        elif pos_tag[1].startswith('V'):
            return (pos_tag[0], wordnet.VERB)
        elif pos_tag[1].startswith('N'):
            return (pos_tag[0], wordnet.NOUN)
        elif pos_tag[1].startswith('R'):
            return (pos_tag[0], wordnet.ADV)
        else:
            return (pos_tag[0], wordnet.NOUN)



    def word2vec_similarity(self, w1,w2):

        try:
            w1 = wn.synsets(w1)
            w2 = wn.synsets(w2)
            score = float(w1[0].wup_similarity(w2[0]))
        except Exception:
            score = 0.00
        return score

    def sentence_modifier(self,a):
        # breakdown complex words to simpler ones: footwear -> foot wear
        # using https://github.com/casics/spiral
        ronin.init(low_freq_cutoff=10)

        #tokenization & lemmatization
        pos_a = map(self.get_wordnet_pos, nltk.pos_tag(ronin.split(a)))
        lemmae_a = [self.lemmatizer.lemmatize(token.lower().strip(string.punctuation), pos) for token, pos in pos_a \
                        if token.lower().strip(string.punctuation) not in self.stopwords]
        return " ".join(lemmae_a)

    def similarity(self, a, b, threshold=0.5):
        """Check if a and b are matches."""
        # breakdown complex words to simpler ones: footwear -> foot wear
        # using https://github.com/casics/spiral
        ronin.init(low_freq_cutoff=10)

        #tokenization & lemmatization
        pos_a = map(self.get_wordnet_pos, nltk.pos_tag(ronin.split(a)))
        pos_b = map(self.get_wordnet_pos, nltk.pos_tag(ronin.split(b)))
        lemmae_a = [self.lemmatizer.lemmatize(token.lower().strip(string.punctuation), pos) for token, pos in pos_a \
                        if token.lower().strip(string.punctuation) not in self.stopwords]
        lemmae_b = [self.lemmatizer.lemmatize(token.lower().strip(string.punctuation), pos) for token, pos in pos_b \
                        if token.lower().strip(string.punctuation) not in self.stopwords]

        # Calculate Modified Jaccard similarity
        intersection = set(lemmae_a).intersection(lemmae_b)
        union = set(lemmae_a).union(lemmae_b)
        difference = set(lemmae_a).symmetric_difference(lemmae_b)
        difference_a = set(difference).difference(lemmae_a)
        difference_b = set(difference).difference(lemmae_b)
        if (len(difference_a) != 0) and (len(difference_b) != 0):
            max_score = min(len(difference_a),len(difference_b))
            denominator = len(difference_a)*len(difference_b)
            numerator = 0.00
            for w1 in difference_a:
                for w2 in difference_b:
                    if w1 != w2:
                        sim = self.word2vec_similarity(w1,w2)
                        numerator = numerator + sim
                        # print(w1,w2,sim)
            added_score = (numerator/float(denominator)) * (max_score)
        else:
            max_score = 0
            numerator = 0
            denominator = 1
            added_score = 0.0
        modified_jaccard = (added_score + len(intersection))/float(len(union) - added_score)
        # modified_jaccard = (added_score + len(intersection))/float(len(union) - added_score)
        # debugging

        print("lemmae_a ",lemmae_a)
        print("lemmae_b ",lemmae_b)
        print("intersection ",intersection)
        print("union ",union)
        print("difference ",difference)
        print("difference_a ",difference_a)
        print("difference_b ",difference_b)
        print("max_score ",max_score)
        print("numerator ",numerator)
        print("denominator ",denominator)
        print("added_score ",added_score)
        print("modified_jaccard ",modified_jaccard)


        return (modified_jaccard)

        #think about pairwise similarity impact on unition "slim_thin_closure_type", "foot_leg_closure_type"


    def find_nearest(self,target):
        target = target

        max_score = 0
        match = ""
        for line in  self.dictionary:
          word = str(line.strip())
          score = self.similarity(target, word)
          if (score > max_score):
              match = word
              max_score = score
        l  =[target, match, max_score]
        print(l)
        return(l)

    def find_nearest_cosine(self,target):
        temp_dict = []
        for dict in self.dictionary:
            temp_dict.append(self.sentence_modifier(dict))
        target = self.sentence_modifier(target)

        temp_dict.insert(0,target)

        #calculating pairwise similarity
        #vectorizer with l2 norm
        tfidf=text.TfidfVectorizer(input=temp_dict,stop_words="english", norm="l2")
        matrix=tfidf.fit_transform(temp_dict)
        similarity_matrix=cosine_similarity(matrix)

        #similarity of target with corpus
        target_similarity = similarity_matrix[0]
        #zeroing 1st element
        target_similarity[0] = 0.0
        max_index = np.argmax(target_similarity)
        most_similar_sentence = self.dictionary[max(0,max_index-1)]
        max_score = target_similarity[max_index]
        return ([target,most_similar_sentence,max_score])

    def run(self):
      output = []
      targets = self.target_file
      dictionary = self.dictionary_file
      with open(targets, 'r') as csvfile_t:
          targetcsv = csv.reader(csvfile_t, delimiter=',')
          with open(dictionary, 'r') as csvfile_d:
              dictcsv = csv.reader(csvfile_d, delimiter=',')
              self.targets = []
              self.dictionary = []

              for target in targetcsv:
                  self.targets.append(target[0])
              for dict in dictcsv:
                  self.dictionary.append(dict[0])
              #transforming WordNet
              print (wn.__class__)           # <class 'nltk.corpus.util.LazyCorpusLoader'>
              wn.ensure_loaded()            # first access to wn transforms it
              print (wn.__class__)          # <class 'nltk.corpus.reader.wordnet.WordNetCorpusReader'>

              pool = ThreadPool(self.threads)
              output = pool.map(self.find_nearest_cosine,self.targets)
              pool.close()
              pool.join()


      with open(self.output_file, 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=',')
            for out in output:
                writer.writerow(out)
      print("-------- Finished --------")
