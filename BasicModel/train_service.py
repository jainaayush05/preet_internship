#!/usr/bin/python

# -*- coding: utf-8 -*-

from dd_client import DD



host = '127.0.0.1'

sname = 'greendeck'

description = 'image classification'

mllib = 'caffe'

dd = DD(host)

dd.set_return_format(dd.RETURN_PYTHON)



# training / finetuning from pre-trained network



train_data = ['/home/dd/images']

parameters_input = {'test_split': 0.1, 'shuffle': True}

#overrides solver

parameters_mllib = {'gpu': False, 'net': {'batch_size': 64,

                    'test_batch_size': 64}, 'solver': {

    'test_interval': 2000,

    'iterations': 20000,

    'snapshot': 10000,

    'base_lr': 0.01,

    'solver_type': 'NESTEROV',

    'test_initialization': True,

    }}

parameters_output = {'measure': ['mcll', 'f1', 'acc-5']}

dd.post_train(

    sname,

    train_data,

    parameters_input,

    parameters_mllib,

    parameters_output,

    async=True,

    )



# time.sleep(1)

train_status = ''

while True:

    train_status = dd.get_train(sname, job=1, timeout=10)

    if train_status['head']['status'] == 'running':

        print(train_status['body']['measure'])

    else:

        print(train_status)

        break
