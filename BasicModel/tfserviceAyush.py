from dd_client import DD



model_repo = '/home/dd/models/clothing'

height = width = 224

nclasses = 10



# setting up DD client

host = '127.0.0.1'

sname = 'greendeck'

description = 'image classification'

mllib = 'caffe'

dd = DD(host)

dd.set_return_format(dd.RETURN_PYTHON)



# creating ML service

model = {'repository':model_repo}

parameters_input = {'connector':'image','width':width,'height':height}

# parameters_mllib = {'template':'googlenet','nclasses':nclasses,'finetuning':True,'rotate':False,'mirror':True,'weights':'model_iter_300000.caffemodel'}
parameters_mllib = {'nclasses':nclasses,'finetuning':True,'rotate':False,'mirror':True,'weights':'model_iter_300000.caffemodel'}

parameters_output = {}

dd.put_service(sname,model,description,mllib,parameters_input,parameters_mllib,parameters_output)
