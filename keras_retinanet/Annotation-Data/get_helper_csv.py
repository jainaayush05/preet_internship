import pandas as pd

file = 'products_no_11street.csv'

final_address = []
final_url = []
final_web_id = []
final_image_id = []

web_id = []
image_url0=[]
image_url1=[]
image_url2=[]
image_url3=[]
image_url4=[]
image_url5=[]
image_id=[]
l1=[]
l2=[]
l3=[]
l4=[]

for chunk in pandas.read_csv(file,chunksize=20000):

	image_url0_chunk = (chunk.iloc[:,4])
	for element in image_url0_chunk:
		image_url0.append(element)

	image_url1_chunk = (chunk.iloc[:,5])
	for element in image_url1_chunk:
		image_url1.append(element)

	image_url2_chunk = (chunk.iloc[:,6])
	for element in image_url2_chunk:
		image_url2.append(element)

	image_url3_chunk = (chunk.iloc[:,7])
	for element in image_url3_chunk:
		image_url3.append(element)

	image_url4_chunk = (chunk.iloc[:,8])
	for element in image_url4_chunk:
		image_url4.append(element)

	image_url5_chunk = (chunk.iloc[:,9])
	for element in image_url5_chunk:
		image_url5.append(element)

	image_id_chunk = (chunk.iloc[:,0])
	for element in image_id_chunk:
		image_id.append(element)

	web_id_chunk = (chunk.iloc[:,1])
	for element in web_id_chunk:
		web_id.append(element)

	l3_chunk = (chunk.iloc[:,13])
	for element in l3_chunk:
		l3.append(element)

	l2_chunk = (chunk.iloc[:,12])
	for element in l2_chunk:
		l2.append(element)

	l4_chunk = (chunk.iloc[:,14])
	for element in l4_chunk:
		l4.append(element)

	l1_chunk = (chunk.iloc[:,11])
	for element in l1_chunk:
		l1.append(element)

for i in range(len(image_id)):

	this_image_url0 = image_url0[i]
	this_image_url1 = image_url1[i]
	this_image_url2 = image_url2[i]
	this_image_url3 = image_url3[i]
	this_image_url4 = image_url4[i]
	this_image_url5 = image_url5[i]
	this_id = image_id[i]
	this_l1 = l1[i]
	this_l2 = l2[i]
	this_l3 = l3[i]
	this_l4 = l4[i]
	this_web_id = web_id[i]

	this_address = ""
	is_finished = False

	try:
		this_l1 = this_l1.strip()
		if (len(this_l1) > 1):
			this_address += this_l1
		else:
			is_finished = True
	except:
		is_finished = True

	if (is_finished == False):
		try:
			this_l2 = this_l2.strip()
			if (len(this_l2) > 1):
				this_address += '/' + this_l2
			else:
				is_finished = True
		except:
			is_finished = True

	if (is_finished == False):
		try:
			this_l3 = this_l3.strip()
			if (len(this_l3) > 1):
				this_address += '/' + this_l3
			else:
				is_finished = True
		except:
			is_finished = True

	if (is_finished == False):
		try:
			this_l4 = this_l4.strip()
			if (len(this_l4) > 1):
				this_address += '/' + this_l4
			else:
				is_finished = True
		except:
			is_finished = True

	if (len(this_address) == 0):
		this_address = "No Label"
		continue

	try:
		this_image_url0 = this_image_url0.strip()
		final_address.append(this_address)
		final_url.append(this_image_url0)
		final_image_id.append(this_id+'#0')
		final_web_id.append(this_web_id)
	except:
		pass

	try:
		this_image_url1 = this_image_url1.strip()
		final_address.append(this_address)
		final_url.append(this_image_url1)
		final_image_id.append(this_id+'#1')
		final_web_id.append(this_web_id)
	except:
		pass

	try:
		this_image_url2 = this_image_url2.strip()
		final_address.append(this_address)
		final_url.append(this_image_url2)
		final_image_id.append(this_id+'#2')
		final_web_id.append(this_web_id)
	except:
		pass

	try:
		this_image_url3 = this_image_url3.strip()
		final_address.append(this_address)
		final_url.append(this_image_url3)
		final_image_id.append(this_id+'#3')
		final_web_id.append(this_web_id)
	except:
		pass

	try:
		this_image_url4 = this_image_url4.strip()
		final_address.append(this_address)
		final_url.append(this_image_url4)
		final_image_id.append(this_id+'#4')
		final_web_id.append(this_web_id)
	except:
		pass

	try:
		this_image_url5 = this_image_url5.strip()
		final_address.append(this_address)
		final_url.append(this_image_url5)
		final_image_id.append(this_id+'#5')
		final_web_id.append(this_web_id)
	except:
		pass

file = open('url_helper_bbox_anno.csv','w')

for i in range(len(final_url)):

	file.write(final_image_id[i]+','+final_web_id[i]+','+final_address[i]+','+final_url[i]+'\n')

file.close()