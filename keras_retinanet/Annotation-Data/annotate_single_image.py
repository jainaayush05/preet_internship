import requests

url = "http://catalog.markable.ai/image/search"

querystring = {"endpointDrishti":"http://samsonite.markable.ai"}

# payload = "{\"data\":{\"image_uri\":\"https://mosaic01.ztat.net/vgs/media/packshot/pdp-zoom/TT/25/4H/01/4J/11/TT254H014-J11@1.1.jpg\",\"catalogs\":[{\"_id\":\"5a1bc5c2f976cd6b4d52cdd2\"},{\"_id\":\"5a13da2b4f0e8d10f6e92edd\"},{\"_id\":\"5a13cd83ffe89b68c3340774\"},{\"_id\":\"5a1bbbdf45ed1519a4f5262d\"},{\"_id\":\"59baba11b9bdda77fe76a614\"},{\"_id\":\"5a1bc55c8cb1f97f54a55384\"},{\"_id\":\"5a13d7994f0e8d10f6e92ae2\"},{\"_id\":\"59e90a6a1a2d3d753f55fe0a\"},{\"_id\":\"59f8ae779935f46d12ac8fcf\"},{\"_id\":\"5a2d3afa69d90c1135eff1d0\"}],\"gender\":[],\"attributes\":true}}"
# payload = '{"data":{"image_uri":"https://sc02.alicdn.com/kf/HTB1.JYBRpXXXXcHaFXXq6xXFXXXi/Beautiful-korean-ladies-pu-hand-bags-women.jpg_350x350.jpg","catalogs":[{"_id":"5a1bc5c2f976cd6b4d52cdd2"},{"_id":"5a13da2b4f0e8d10f6e92edd"},{"_id":"5a13cd83ffe89b68c3340774"},{"_id":"5a1bbbdf45ed1519a4f5262d"},{"_id":"59baba11b9bdda77fe76a614"},{"_id":"5a1bc55c8cb1f97f54a55384"},{"_id":"5a13d7994f0e8d10f6e92ae2"},{"_id":"59e90a6a1a2d3d753f55fe0a"},{"_id":"59f8ae779935f46d12ac8fcf"},{"_id":"5a2d3afa69d90c1135eff1d0"}],"gender":[],"attributes":true}}'

this_url = 'https://sc02.alicdn.com/kf/HTB1.JYBRpXXXXcHaFXXq6xXFXXXi/Beautiful-korean-ladies-pu-hand-bags-women.jpg_350x350.jpg'
# payload = payload = '{"data":{"image_uri":'+this_url+',"catalogs":[{"_id":"5a1bc5c2f976cd6b4d52cdd2"},{"_id":"5a13da2b4f0e8d10f6e92edd"},{"_id":"5a13cd83ffe89b68c3340774"},{"_id":"5a1bbbdf45ed1519a4f5262d"},{"_id":"59baba11b9bdda77fe76a614"},{"_id":"5a1bc55c8cb1f97f54a55384"},{"_id":"5a13d7994f0e8d10f6e92ae2"},{"_id":"59e90a6a1a2d3d753f55fe0a"},{"_id":"59f8ae779935f46d12ac8fcf"},{"_id":"5a2d3afa69d90c1135eff1d0"}],"gender":[],"attributes":true}}'
payload = "{\"data\":{\"image_uri\":\""+ this_url + "\",\"catalogs\":[{\"_id\":\"5a1bc5c2f976cd6b4d52cdd2\"},{\"_id\":\"5a13da2b4f0e8d10f6e92edd\"},{\"_id\":\"5a13cd83ffe89b68c3340774\"},{\"_id\":\"5a1bbbdf45ed1519a4f5262d\"},{\"_id\":\"59baba11b9bdda77fe76a614\"},{\"_id\":\"5a1bc55c8cb1f97f54a55384\"},{\"_id\":\"5a13d7994f0e8d10f6e92ae2\"},{\"_id\":\"59e90a6a1a2d3d753f55fe0a\"},{\"_id\":\"59f8ae779935f46d12ac8fcf\"},{\"_id\":\"5a2d3afa69d90c1135eff1d0\"}],\"gender\":[],\"attributes\":true}}"
headers = {
    'authorization': "Bearer 5991c086c17de50e1e2e1d66",
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "dc52c70b-7451-ca9d-7f5e-ecccc15dd608"
    }

response = requests.request("POST", url, data=payload, headers=headers, params=querystring)

print(response.text)