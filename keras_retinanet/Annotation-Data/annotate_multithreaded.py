import requests
from multiprocessing.dummy import Pool as ThreadPool
import os

def get_response(this_url):

	url = "http://catalog.markable.ai/image/search"

	querystring = {"endpointDrishti":"http://samsonite.markable.ai"}

	payload = "{\"data\":{\"image_uri\":\""+ this_url + "\",\"catalogs\":[{\"_id\":\"5a1bc5c2f976cd6b4d52cdd2\"},{\"_id\":\"5a13da2b4f0e8d10f6e92edd\"},{\"_id\":\"5a13cd83ffe89b68c3340774\"},{\"_id\":\"5a1bbbdf45ed1519a4f5262d\"},{\"_id\":\"59baba11b9bdda77fe76a614\"},{\"_id\":\"5a1bc55c8cb1f97f54a55384\"},{\"_id\":\"5a13d7994f0e8d10f6e92ae2\"},{\"_id\":\"59e90a6a1a2d3d753f55fe0a\"},{\"_id\":\"59f8ae779935f46d12ac8fcf\"},{\"_id\":\"5a2d3afa69d90c1135eff1d0\"}],\"gender\":[],\"attributes\":true}}"
	
	headers = {
	    'authorization': "Bearer 5991c086c17de50e1e2e1d66",
	    'content-type': "application/json",
	    'cache-control': "no-cache",
	    'postman-token': "dc52c70b-7451-ca9d-7f5e-ecccc15dd608"
	    }

	response = requests.request("POST", url, data=payload, headers=headers, params=querystring)
	return (response.text)

def parse_and_run(c):

	c = c.strip()

	contents = c.split(',')
	try:
		this_id = contents[0]
		this_url = contents[3]

		if (os.path.exists('api_response/'+this_id+'.txt')):
			print (this_id+' already exists, not getting the response')
			pass

		else:
			file = open('api_response/'+this_id+'.txt','w')
			print ('getting response for '+this_id)
			api_response = get_response(this_url)
			# print (api_response)
			file.write(api_response)
			file.close()

	except Exception as e:
		raise
		print("ERROR: "+str(e))


def get_anno_from_url(helper_file_name):

	with open(helper_file_name,'r') as f:
		contents  = f.readlines()

	try:
		pool = ThreadPool(40)
		pool.map(parse_and_run, contents)
		pool.close()
		pool.join()
	except Exception as e:
		raise
		print("ERROR: "+str(e))

def main():
	helper_file_name = 'url_helper_bbox_anno.csv'
	get_anno_from_url(helper_file_name)

if __name__ == "__main__":
	main()